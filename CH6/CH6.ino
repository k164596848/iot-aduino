static int ledStatus;
static unsigned long lastDebounceTime;
#define DEBOUNCE_DELAY 200
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(12,INPUT);//D6=12
  pinMode(13,OUTPUT);//D7=13
  pinMode(15,OUTPUT);//D8=15
  ledStatus = LOW;
  digitalWrite(13,ledStatus);

}
void updateLed(){
  unsigned long currentTime =millis(); 
  if ((currentTime - lastDebounceTime)>DEBOUNCE_DELAY){
    lastDebounceTime = currentTime;
    ledStatus = ledStatus == HIGH ? LOW :HIGH;
    digitalWrite(13,ledStatus);
    Serial.print(ledStatus);
    
  }
  
}
void ambulance(int pin){
  if (ledStatus == HIGH){
    tone(15,950,400);
    delay(400);    
    tone(15,750,400);
    delay(400);
  }
  else
  {
    noTone(15);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  int switchStatus =digitalRead(12);
  if(switchStatus == HIGH){
  updateLed(); 
  }
  ambulance(15);    
}
