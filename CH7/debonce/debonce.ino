static int ledStatus;
static unsigned long lastDebounceTime;
#define DEBOUNCE_DELAY 200
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(12,INPUT);
  pinMode(13,OUTPUT);
  ledStatus = LOW;
  digitalWrite(13,ledStatus);
}
void updateLed(){
  unsigned long currentTime =millis(); 
  if ((currentTime - lastDebounceTime)>DEBOUNCE_DELAY){
    lastDebounceTime = currentTime;
    ledStatus = ledStatus == HIGH ? LOW :HIGH;
    digitalWrite(13,ledStatus);
    Serial.print(ledStatus);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  int switchStatus =digitalRead(12);
  if(switchStatus == HIGH){
    updateLed();
  }
}
