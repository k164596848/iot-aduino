static int ledStatus;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(15,INPUT);
  pinMode(13,OUTPUT);

  ledStatus =LOW;
  digitalWrite(13,ledStatus);

}

void loop() {
  // put your main code here, to run repeatedly:
  int switchStatus = digitalRead(15);
  if (switchStatus == HIGH)
  {
    ledStatus = ledStatus == HIGH?LOW:HIGH;
    digitalWrite(13,ledStatus);
  }
  Serial.print(ledStatus);
}
