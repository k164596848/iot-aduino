static int ledStatus;
static int switchStatus;
void setup() {
  // put your setup code here, to run once:
  Serial.begin (115200);
  pinMode(D8,INPUT);//讀取按鈕按壓
  pinMode(D7,OUTPUT);//LED輸出
  pinMode(D6,OUTPUT);//蜂鳴器輸出

  ledStatus = LOW;
  digitalWrite(D7,ledStatus);
}

void loop() {
  // put your main code here, to run repeatedly:
  switchStatus = digitalRead(D8);
  int judege =ledStatus;
  if (switchStatus == HIGH)//是否按下按鈕
  {
    ledStatus = ledStatus == HIGH ? LOW :HIGH;
  }
  digitalWrite(D7,ledStatus);
 
  if (judege!= ledStatus)//判定變換狀態，有，就延長
  {
    if (ledStatus ==HIGH)//判定狀態為HIGH，有，就響聲
    {
      tone(D6,262,00);
    }
    delay(300);
  }  
  delay(100); 
  noTone(D6);
  Serial.print(ledStatus);
}
