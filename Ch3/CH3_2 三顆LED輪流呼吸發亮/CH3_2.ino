int brightness = 0;
int fadeAmount =5;
int delayDuration =30;
int judge = 0;

void Control()
{
  brightness =brightness + fadeAmount;
  if (brightness <=0 || brightness >=225)
  {
    fadeAmount = -fadeAmount;
  }  
}
void setup() {
  // put your setup code here, to run once:
  pinMode(D1,OUTPUT);
  pinMode(D2,OUTPUT);
  pinMode(D3,OUTPUT);

}



void loop() {
  // put your main code here, to run repeatedly:
  if (judge%3==0)
  {
    analogWrite(D1,brightness);
    Control();
    if (brightness==0)judge++;
  }
  else if(judge%3==1)
  {
    analogWrite(D2,brightness);
    Control();
    if (brightness==0)judge++;
  }
  else if (judge%3==2)
  {
    analogWrite(D3,brightness);
    Control();
    if (brightness==0)judge++;
  }
  if(judge==3)judge=0;
  delay(delayDuration);

}
