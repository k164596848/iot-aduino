int brightness = 0;
int fadeAmount =5;
int delayDuration =30;
void setup() {
  // put your setup code here, to run once:
  pinMode(D1,OUTPUT);
  pinMode(D2,OUTPUT);
  pinMode(D3,OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  analogWrite(D1,brightness);
  brightness =brightness + fadeAmount;

  if (brightness <=0 || brightness >=225)
  {
    fadeAmount = -fadeAmount;
  }
  delay(delayDuration);

}
