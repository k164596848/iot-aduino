static int led_GreenStatus;
static int switchStatus;

void setup() {
  // put your setup code here, to run once:
  Serial.begin (115200);
  pinMode(D8,INPUT);//讀取按鈕按壓
  pinMode(D7,OUTPUT);//LED_Green輸出
  pinMode(D5,OUTPUT);//LED_Red輸出
  pinMode(D6,OUTPUT);//蜂鳴器輸出

  led_GreenStatus = LOW;
  digitalWrite(D7,led_GreenStatus);
}

void loop() {
  // put your main code here, to run repeatedly:
  switchStatus = digitalRead(D8);
  int judgeStatus =led_GreenStatus;
  if (switchStatus == HIGH)//是否按下按鈕
  {
    led_GreenStatus = led_GreenStatus == HIGH ? LOW :HIGH;
  }
  digitalWrite(D7,led_GreenStatus);
  if (led_GreenStatus ==HIGH)//判定LED綠為HIGH，有，止聲
  {
      noTone(D6);digitalWrite(D5,LOW);    
  }  
  else//判定狀態為LOW，有，就響聲且紅綠LED閃爍
  {
    tone(D6,262);
    digitalWrite(D7,HIGH);digitalWrite(D5,LOW);
    delay(120);
    digitalWrite(D7,LOW);digitalWrite(D5,HIGH);
    delay(120);
  }
  if  (judgeStatus!= led_GreenStatus)delay(185);
}
