int buzzerPin =14;
char val;

void alarmDo(int pin)
{
  tone(pin,262,5000);
}
void alarmRe(int pin)
{
  tone(pin,294,500);
}
void alarmMi(int pin)
{
  tone(pin,330,500);
}
void alarmFa(int pin)
{
  tone(pin,349,500);
}
void alarmSo(int pin)
{
  tone(pin,392,500);
}
void alarmLa(int pin)
{
  tone(pin,440,500);
}
void alarmSi(int pin)
{
  tone(pin,493,500);
}
void alarm(int pin)
{
  tone(pin,523,500);
}
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(buzzerPin,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available())
  {
    val=Serial.read();
  }
  if(val =='1')
  {
    alarmDo(buzzerPin);
  }
  else if(val=='2')
  {
    alarmRe(buzzerPin);
  }
  else if(val=='3')
  {
    alarmMi(buzzerPin);   
  }
  else if(val=='4')
  {
    alarmFa(buzzerPin);
  }
  else if(val=='5')
  {
    alarmSo(buzzerPin);
  }
  else if(val=='6')
  {
    alarmLa(buzzerPin);
  }else if(val=='7')
  {
    alarmSi(buzzerPin);
  }
  else if(val=='8')
  {
    alarm(buzzerPin);
  }
  else
  {
    noTone(buzzerPin);
  }
  delay(500);
  val='s';
}
