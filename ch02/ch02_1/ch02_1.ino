char val;
int ledPin = D1;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(ledPin,OUTPUT);
  pinMode(D2,OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:

  if(Serial.available())
  {
    val= Serial.read();
  }
  if(val =='a')//按著a亮D1的燈
  {
    ledPin = D1;
    digitalWrite(ledPin,HIGH);
  }
  else if (val=='b')//按著b亮D2的燈
  {
    ledPin=D2;
    digitalWrite(ledPin,HIGH);
  }
  else if (val=='c')//按著c關閉上一個亮的燈
  {
    digitalWrite(ledPin,LOW);
  }

  delay(1000);
  

}
